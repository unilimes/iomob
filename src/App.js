import React, {Component} from 'react';
import MapGL, {FlyToInterpolator} from 'react-map-gl';

import DeckGLOverlay from './deckgl-overlay.js';
import ControlsPanel from './components/controls-panel';
import InfoOverlay from './components/info-overlay';
import InfoTimeOverlay from './components/info-time-overlay';

import busStations from './data/stations.json';
import busLines from './data/bus-lines.json';
import trips from './data/testTrips.json';
import metroStations from './data/metro-stations.json';
import metroEdges from './data/metro-edges.json';
import bicingStations from './data/bicing-stations.json';


import './App.css';

// Set your mapbox token here
// const MAPBOX_TOKEN = process.env.MapboxAccessToken; // eslint-disable-line
const MAPBOX_TOKEN = 'pk.eyJ1Ijoic29sb21rYSIsImEiOiJjamczbTFvMG02c3hlMzJudjNvbWp6ZXZmIn0.QDxTgWxuwqlNa6Tvk1VCmQ'; // eslint-disable-line

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                ...DeckGLOverlay.defaultViewport,
                width: 500,
                height: 500
            },
            trips: null,
            time: 0,
            selectedStation: null,
            hoveredStation: null,
            metroLines: [],
            selectedBusLine: null,
            info: {
                transport: [
                    {name: 'bus', color: 'rgba(45, 138, 255, 1)'},
                    {name: 'train', color: 'rgba(255, 60, 60, 1)'},
                    {name: 'taxi', color: 'rgba(0, 171, 19, 1)'},
                    {name: 'bike', color: 'rgba(226, 60, 230, 1)'},
                ]
            }
        };
    }

    componentDidMount() {
        this.setState({trips});
        this.setState({busStations});
        this.setState({busLines});

        function fromLL(lon,lat) {
            // derived from https://gist.github.com/springmeyer/871897
            var extent = 20037508.34;

            var x = lon * extent / 180;
            var y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
            y = y * extent / 180;

            return [(x + extent) / (2 * extent), 1 - ((y + extent) / (2 * extent))];
        }
        var translate = fromLL(148.98190, -35.39847);

        var transform = {
            translateX: translate[0],
            translateY: translate[1],
            translateZ: 0,
            rotateX: Math.PI / 2,
            rotateY: 0,
            rotateZ: 0,
            scale: 5.41843220338983e-8
        };
        var THREE = window.THREE;

        var threeJSModel = {
            id: 'custom_layer',
            type: 'custom',
            onAdd: function(map, gl) {
                console.log('inited');
                this.camera = new THREE.Camera();
                this.scene = new THREE.Scene();

                var directionalLight = new THREE.DirectionalLight(0xffffff);
                directionalLight.position.set(0, -70, 100).normalize();
                this.scene.add(directionalLight);

                var directionalLight2 = new THREE.DirectionalLight(0xffffff);
                directionalLight2.position.set(0, 70, 100).normalize();
                this.scene.add(directionalLight2);

                var loader = new THREE.GLTFLoader();
                loader.load('./data/model.gltf', (function (gltf) {
                    this.scene.add(gltf.scene);
                }).bind(this));
                this.map = map;

                this.renderer = new THREE.WebGLRenderer({
                    canvas: map.getCanvas(),
                    context: gl
                });

                this.renderer.autoClear = false;
            },
            render: function(gl, matrix) {
                var rotationX = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(1, 0, 0), transform.rotateX);
                var rotationY = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 1, 0), transform.rotateY);
                var rotationZ = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 0, 1), transform.rotateZ);

                var m = new THREE.Matrix4().fromArray(matrix);
                var l = new THREE.Matrix4().makeTranslation(transform.translateX, transform.translateY, transform.translateZ)
                    .scale(new THREE.Vector3(transform.scale, -transform.scale, transform.scale))
                    .multiply(rotationX)
                    .multiply(rotationY)
                    .multiply(rotationZ);

                this.camera.projectionMatrix.elements = matrix;
                this.camera.projectionMatrix = m.multiply(l);
                this.renderer.state.reset();
                this.renderer.render(this.scene, this.camera);
                this.map.triggerRepaint();
            }
        };


        const map = this.reactMap.getMap();
        map.on('load', () => {
            const layers = map.getStyle().layers;

            let labelLayerId;
            for (let i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol' && layers[i].layout['text-field']) {
                    labelLayerId = layers[i].id;
                    break;
                }
            }
            map.addLayer({
                'id': '3d-buildings',
                'source': 'composite',
                'source-layer': 'building',
                'filter': ['==', 'extrude', 'true'],
                'type': 'fill-extrusion',
                'paint': {
                    'fill-extrusion-color': '#666',
                    'fill-extrusion-height': [
                        "interpolate", ["linear"], ["zoom"],
                        15, ["get", "height"],
                        17, 20
                    ],
                    'fill-extrusion-opacity': .4
                }
            }, labelLayerId);

            map.addLayer(threeJSModel, 'waterway-label');
        });

        window.addEventListener('resize', this._resize.bind(this));
        this._resize();
        this._animate();
    }

    componentWillUnmount() {
        if (this._animationFrame) {
            window.cancelAnimationFrame(this._animationFrame);
        }
    }

    _animate() {
        const timestamp = Date.now();
        const loopLength = 1800;
        const loopTime = 180000;

        this.setState({
            time: (timestamp % loopTime) / loopTime * loopLength
        });
        this._animationFrame = window.requestAnimationFrame(this._animate.bind(this));
    }

    _resize() {
        this._onViewportChange({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    _onHover(info) {
        if (!info.picked) {
            this.setState({hoveredStation: null, metroLines: []});
        } else {
            let type;
            switch (info.layer.id) {
                case 'metro-stations-layer':
                    type = 'metro-station';
                    this.setMetroLine(info.object.line);
                    break;
                case 'bus-stations-layer':
                    type = 'bus-station';
                    break;
                case 'bicing-stations-layer':
                    type = 'bicing-station';
                    break;
                default:
                    return;
            }
            this.setState({hoveredStation: {type, x: info.x, y: info.y, ...info.object}});
        }
    }

    _onClick(info) {
        let type;
        switch (info.layer.id) {
            case 'metro-stations-layer':
                type = 'metro-station';
                break;
            case 'bus-stations-layer':
                type = 'bus-station';
                break;
            default:
                return;
        }
        this.setState({selectedStation: {type, ...info.object}});
        console.log(this.state.selectedStation);
        this.setState({
            viewport: {
                ...this.state.viewport,
                longitude: +info.object.lon,
                latitude: +info.object.lat,
                zoom: 16,
                transitionInterpolator: new FlyToInterpolator(),
                transitionDuration: 500
            }
        });
    }

    _renderPopup() {
        if (!this.state.hoveredStation) return;
        const {type} = this.state.hoveredStation;

        if (type === 'metro-station') {
            let {name, x, y, line, connections} = this.state.hoveredStation;
            connections = connections ? connections.split('-') : [];
            return (
                <div className={'station-popup'} style={{top: y + 10, left: x + 10}}>
                    <div className="popup-title">
                        <span>{line}</span><h4>{name}</h4>
                    </div>
                    {
                        connections.length ?
                            <div className="transport-list">
                                {
                                    connections.map((item, i) => {
                                        return (
                                            <span className='line-list-item' key={i}>{item}</span>
                                        );
                                    })
                                }
                            </div> : null
                    }
                </div>
            );
        } else if (type === 'bus-station') {
            let {street_name: name, x, y, buses} = this.state.hoveredStation;
            buses = buses.split(' - ');
            return (
                <div className={'station-popup'} style={{top: y + 10, left: x + 10}}>
                    <div className="popup-title">
                        <h4>{name}</h4>
                    </div>
                    {
                        buses.length ?
                            <div className="transport-list">
                                {
                                    buses.map((item, i) => {
                                        return (
                                            <span className='line-list-item' key={i}>{item}</span>
                                        );
                                    })
                                }
                            </div> : null
                    }
                </div>
            );
        } else if (type === 'bicing-station') {
            let {name, x, y} = this.state.hoveredStation;
            return (
                <div className={'station-popup'} style={{top: y + 10, left: x + 10}}>
                    <div className="popup-title">
                        <h4>{name}</h4>
                    </div>
                </div>
            );
        }
    }

    _closeOptionsPanel() {
        this.setState({selectedStation: null, metroLines: [], selectedBusLine: null});
    }

    setMetroLine(lineName) {
        this.setState({
            metroLines: metroEdges.filter(item => item.line === lineName)
        })
    }

    setBusLine(lineName) {
        this.setState({selectedBusLine: lineName});
    }

    _onViewportChange(viewport) {
        this.setState({
            viewport: {...this.state.viewport, ...viewport}
        });
    }

    render() {
        const {viewport, trips, time, metroLines, selectedStation} = this.state;

        return (
            <div className={'main'}>
                <ControlsPanel
                    selectedStation={selectedStation}
                    setBusLine={this.setBusLine.bind(this)}
                    setMetroLine={this.setMetroLine.bind(this)}
                    closeTransportInfo={this._closeOptionsPanel.bind(this)}
                />
                <InfoOverlay/>
                <InfoTimeOverlay info={this.state.info}/>
                <MapGL
                    ref={(reactMap) => { this.reactMap = reactMap; }}
                    {...viewport}
                    mapStyle="mapbox://styles/mapbox/dark-v9"
                    onViewportChange={this._onViewportChange.bind(this)}
                    mapboxApiAccessToken={MAPBOX_TOKEN}
                >
                    <DeckGLOverlay
                        viewport={viewport}
                        trips={trips}
                        busStations={busStations}
                        metroStations={metroStations}
                        bicingStations={bicingStations}
                        metroLines={metroLines}
                        trailLength={80}
                        time={time}
                        onClick={this._onClick.bind(this)}
                        onHover={this._onHover.bind(this)}
                    />

                </MapGL>
                {this._renderPopup()}
            </div>
        );
    }
}

export default App;
