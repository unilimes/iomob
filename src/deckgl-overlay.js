import React, {Component} from 'react';
import DeckGL, {PathLayer, IconLayer} from 'deck.gl';
import TripsLayer from './components/trips-layer';

const METRO_LINES_COLORS = {
    'L1': [219, 61, 77], // '#db3d4d'
    'L2': [133, 70, 147], // '#854693',
    'L3': [55, 170, 93], // '#37aa5d',
    'L4': [243, 187, 42], // '#f3bb2a',
    'L5': [13, 119, 179], // '#0d77b3',
    'L6': [113, 127, 183], // '#717fb7',
    'L7': [164, 87, 19], // '#a45713',
    'L8': [216, 133, 177], // '#d885b1',
    'L9': [234, 141, 20], // '#ea8d14',
    'L10': [63, 184, 237], // '#3fb8ed',
    'L11': [165, 200, 115], // '#a5c873',
    'default': [55, 216, 180], // '#37d8b4',
};

const ICON_MAPPING = {
  bus: {x: 0, y: 0, width: 64, height: 64, mask: true},
  busLine: {x: 64, y: 0, width: 64, height: 64, mask: false},
  metro: {x: 0, y: 0, width: 64, height: 48, mask: false},
  bicyng: {x: 0, y: 0, width: 64, height: 64, mask: true},
};

function getMetroPath(path) {
  return path.coordinates.split(' ').reduce((fullArr, item) => {
    item = item.split(',');
    if (!item[0] || !item[1]) return fullArr;
    else return fullArr.concat([[item[0], item[1]]]);
  }, []);
}

function getMetroLineColor(station) {
    let value = station.line.split('|')[0];
    if (METRO_LINES_COLORS[value]) return METRO_LINES_COLORS[value];
    else return METRO_LINES_COLORS['default'];
}

function getBusIcon(stop, selectedLine) {
    let lines = stop.buses.split(' - ');
    if (~lines.indexOf(selectedLine)) return 'busLine';
    else return 'bus';
}

export default class DeckGLOverlay extends Component {
  static get defaultViewport() {
    return {
      longitude: 148.98190,
      // longitude: 2.170186,
      latitude: -35.39847,
      // latitude: 41.390257,
      zoom: 16,
      maxZoom: 20,
      pitch: 45,
      bearing: 0
    };
  }

  render() {
    const {viewport, trips, busStations, metroStations, bicingStations, metroLines, trailLength, time, selectedBusLine} = this.props;

    if (!trips) {
      return null;
    }

    const layers = [
      new TripsLayer({
        id: 'trips',
        data: trips,
        getPath: d => d.segments,
        getColor: d => (d.type === 'bus' ? [45, 138, 255] : d.type === 'taxi' ? [0, 171, 19] : d.type === 'metro' ? [255, 60, 60] : [226, 60, 230]),
        opacity: 1,
        strokeWidth: 4,
        trailLength,
        currentTime: time,
      }),
      new PathLayer({
          id: 'metro-edges-layer',
          data: metroLines,
          visible: metroLines.length,
          getPath: getMetroPath,
          rounded: false,
          width: 2,
          getColor: getMetroLineColor,
          widthScale: 20,
      }),
      new IconLayer({
          id: 'bus-stations-layer',
          data: busStations,
          pickable: this.props.onHover || this.props.onClick,
          visible: viewport.zoom > 14,
          getSize: f => 30,
          getIcon: f => getBusIcon(f, selectedBusLine),
          getColor: f => [85, 160, 255],
          getPosition: d => [d.lon, d.lat],
          iconAtlas: './images/bus-sprite.png',
          iconMapping: ICON_MAPPING,
          onHover: this.props.onHover,
          onClick: this.props.onClick
      }),
      new IconLayer({
          id: 'metro-stations-layer',
          data: metroStations,
          pickable: this.props.onHover || this.props.onClick,
          visible: viewport.zoom > 12,
          getSize: f => 35,
          getIcon: f => 'metro',
          getPosition: d => [d.lon, d.lat],
          iconAtlas: './images/metro-station-icon.png',
          iconMapping: ICON_MAPPING,
          onHover: this.props.onHover,
          onClick: this.props.onClick
      }),
        new IconLayer({
            id: 'bicing-stations-layer',
            data: bicingStations,
            pickable: this.props.onHover || this.props.onClick,
            visible: viewport.zoom > 15,
            getSize: f => 45,
            // autoHighlight: true,
            getIcon: f => 'bicyng',
            getColor: f => [226, 60, 230],
            getPosition: d => [d.lon, d.lat],
            iconAtlas: './images/bicyng-station.png',
            iconMapping: ICON_MAPPING,
            onHover: this.props.onHover,
            onClick: this.props.onClick
        })
    ];

    return <DeckGL {...viewport} layers={layers} />;
  }
}
