const fs = require('fs');
const path = require('path');
const request = require('request');
const busStations = require('./bus-stations.json');
const busLines = require('./bus-lines.json');
const metroEdges = require('./metro-edges.json');

const BUS_LINES_URL = 'http://barcelonaapi.marcpous.com/bus/lines.json';
const MAPBOX_KEY = 'pk.eyJ1Ijoic29sb21rYSIsImEiOiJjamczbTFvMG02c3hlMzJudjNvbWp6ZXZmIn0.QDxTgWxuwqlNa6Tvk1VCmQ';
const MAX_LINES_AMOUNT = 25;

function randomInteger(min, max) {
    return Math.round(min - 0.5 + Math.random() * (max - min + 1));
}

function getStation(stationId) {
    console.log('getting stations info for line: ', stationId);
    return new Promise((resolve, reject) => {
        request({
            method: 'GET',
            uri: `http://barcelonaapi.marcpous.com/bus/line/id/${stationId}.json`,
            headers: {
                'Content-Type': 'application/json',
            },
            json: true
        }, function (error, response) {
            if (error) {
                reject(error);
            } else {
                resolve(response.body);
            }
        });
    });
}

function getLines() {
    request({
        method: 'GET',
        uri: BUS_LINES_URL,
        headers: {
            'Content-Type': 'application/json',
        },
        json: true
    }, function (error, response) {
        if (error) {
            console.log('ERROR', error);
        } else {
            let allLines = response.body.data.tmbs;
            new Promise((resolve, reject) => {
                Promise.all(allLines.map(item => getStation(item.line)))
                    .then(result => {
                        resolve(result);
                    }).catch(error => {
                        reject(error);
                })
            }).then(result => {
                allLines.forEach((item, i) => {
                    if (result[i].code = 200) {
                        item.stops = result[i].data ? result[i].data.tmb : [];
                    }
                    if (!result[i].data) {
                        console.log('No Line: ', item.line);
                    }
                });
                fs.writeFile('./bus-lines.json', JSON.stringify(allLines), 'utf8', (data) => {
                    console.log('Final JSON is created!\nLength:', allLines.length);
                })
            })
        }
    });
}

function getDirections(coordsArr) {
    coordsArr = coordsArr.join(';');
    let durationStep, toTime = randomInteger(0, 100);
    let url = `https://api.mapbox.com/directions/v5/mapbox/driving/${coordsArr}?geometries=geojson&access_token=${MAPBOX_KEY}`;
    return new Promise((resolve, reject) => {
        request({
            method: 'GET',
            uri: url,
            headers: {
                'Content-Type': 'application/json',
            },
            json: true
        }, function (error, response) {
            if (error) {
                reject(error);
            } else {
                console.log(response.body.message === 'Not Found');
                if (response.body.message === 'Not Found') {
                    resolve({
                        type: 'bus',
                        vendor: 0,
                        segments: [[0,0,0]]
                    });
                    return
                }
                let routes = response.body.routes[0];
                durationStep = 1800 / routes.geometry.coordinates.length;
                let finalTrip = routes.geometry.coordinates.map((item, i) => {
                    item[2] = toTime;
                    toTime += durationStep;
                    // toTime += randomInteger(4, 8) * (routes.distance / routes.duration)
                    return item;
                });
                resolve({
                    type: 'taxi',
                    // type: Math.random() > 0.4 ? 'bus' : 'taxi',
                    vendor: 0,
                    segments: finalTrip
                })
            }
        });
    });
}

// getLines()

function makeStations() {
    let obj = {};
    busStations.forEach(item => {
        obj[item.id] = item;
    })
    fs.writeFile('./bus-stations.json', JSON.stringify(obj), 'utf8', (data) => {
        console.log('Stations JSON is created!');
    })
}

function generateTrips(start, end) {
    return new Promise((resolve, reject) => {
        let lines = busLines.filter(item => item.stops.length);
        let resultArr = [];
        for (let i = start; i < end && i < lines.length; i++) {
            let line = busLines[i];
            let stops = line.stops.filter(item => busStations[item.id]);
            let toArr = reduceLinesAmount(stops.filter(item => item.direction === 'to'), MAX_LINES_AMOUNT);
            let backArr = reduceLinesAmount(stops.filter(item => item.direction === 'back'), MAX_LINES_AMOUNT);

            resultArr.push(getDirections(toArr.map(item => {
                return [busStations[item.id].lon, busStations[item.id].lat];
            })));
            resultArr.push(getDirections(backArr.map(item => {
                return [busStations[item.id].lon, busStations[item.id].lat];
            })));
        }
        Promise.all(resultArr).then(res => {
            resolve(res);
        });
    })
}

/*generateTrips(40, 60).then(result => {
    fs.writeFile('./test-trips.json', JSON.stringify(result), 'utf8', (data) => {
        console.log('Stations JSON is created!\nLength: ' + result.length);
    })
});*/

function reduceLinesAmount(arr, maxAmount) {
    let val = maxAmount / (arr.length + 1);
    let result = val;
    return arr.filter((item, i) => {
        if (i < result) {
            result += val;
            return true
        } else {
            // console.log(i, ' is removed');
            result += val;
            result++;
            return false;
        }
    })
}

function generateMetroTrips() {
    let lineName = 'L5';
    let durationStep, toTime = randomInteger(0, 10);
    let metroPathTrip = metroEdges.reduce((all, item) => {
        if (item.line === lineName) {
            return all.concat(item.coordinates.split(' '))
        } else {
            return all;
        }
    }, []).filter(item => item.length);
    durationStep = 1800 / metroPathTrip.length;

    metroPathTrip = metroPathTrip.map(item => {
        item = item.split(',');
        toTime += durationStep;
        return [+item[0], +item[1], toTime]
    });
    let obj = {
        type: "metro",
        name: lineName,
        vendor: 0,
        segments: metroPathTrip
    }

    fs.writeFile('./test-trips.json', JSON.stringify(obj), 'utf8', (data) => {
        console.log('Stations JSON is created!');
    })
}

// generateMetroTrips();


