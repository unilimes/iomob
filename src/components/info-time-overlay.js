import React, { Component } from 'react';

function getDate() {
	let date = new Date();
	return date.toDateString();
}
function getTime() {
    let date = new Date();
    return date.getHours() + ':' + date.getMinutes();
}

export default (props) => {
	return (
		<div className="InfoTimeOverlay">
			<h1 className="InfoOverlay__Time">{getTime()}</h1>
			<div className="InfoOverlay__Date">
				<span className="DateDetails">{getDate()}</span>
			</div>
			<ul className="InfoOverlay__Transport">
				{
					props.info.transport.map(item => {
						return (
							<span key={item.name} className="transport-type"><span style={{background: `linear-gradient(to right, rgba(0,0,0,0) 0%, ${item.color} 50%, rgba(0,0,0,0) 100%)`}} className="transport-type-color"></span>{item.name}</span>
						)
					})
				}
			</ul>
		</div>
	);
}