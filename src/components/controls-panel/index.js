import React, { Component } from 'react';

import './ControlsPanel.css';

export class ControlsPanel extends Component {

	constructor(props) {
		super(props);
		this.state = {
			selectedTransport: [
				{name: 'metro', selected: true, icon: 'subway'},
				{name: 'bus', selected: true, icon: 'bus'},
				{name: 'bicycle', selected: false, icon: 'bicycle'},
				{name: 'taxi', selected: false, icon: 'taxi'},
			],
			logs: [
				{text: 'Awaiting taxi', status: 'done'},
				{text: 'Riding taxi', status: 'done'},
				{text: 'Walking to metro', status: 'done'},
				{text: 'In metro system', status: 'progress'},
			]
		}
	}

    renderOptionsPanel() {
        const {type} = this.props.selectedStation;
        const {setBusLine, setMetroLine, closeTransportInfo} = this.props;

        if (type === 'metro-station') {
            let {name, line, connections} = this.props.selectedStation;
            connections = connections ? connections.split('-') : [];
            return (
				<div className={'options-panel'}>
					<div>
						<img className="panel-type-icon" src="./images/metro-station-icon.png" alt=""/>
						<h3 className="TransportInfo__Title">{name}</h3>
						<p>Line: <span onClick={() => setMetroLine(line)}
									   className='line-list-item'>{line}</span></p>
					</div>
					{
                        connections.length ?
							<div className="transport-list">Connections:
                                {
                                    connections.map((item, i) => {
                                        return (
											<span className='line-list-item'
												  onClick={() => setMetroLine(item)}
												  key={i}>{item}</span>
                                        );
                                    })
                                }
							</div> : null
                    }
					<button className="close-panel-btn" onClick={() => closeTransportInfo()}><i className="fa fa-times" aria-hidden="true"></i></button>
				</div>
            );
        } else if (type === 'bus-station') {
            let {street_name: name, buses} = this.props.selectedStation;
            buses = buses.split(' - ');
            return (
				<div className={'options-panel'}>
					<div>
						<img className="panel-type-icon" src="./images/bus-stop.png" alt=""/>
						<h3 className="TransportInfo__Title">{name}</h3>
					</div>
					<div className="transport-list"> Bus Lines:
                        {
                            buses.map((item, i) => {
                                return (
									<span onClick={() => setBusLine(item)} className='line-list-item' key={i}>{item}</span>
                                );
                            })
                        }
					</div>
					<button className="close-panel-btn" onClick={() => closeTransportInfo()}><i className="fa fa-times" aria-hidden="true"></i></button>
				</div>
            );
        }
    }

	handleClickOnCategory = (index) => {
		let newArr = this.state.selectedTransport;
		newArr[index].selected = !newArr[index].selected;
		this.setState({selectedTransport: newArr});
	};

	handleAddOriginClick = (e) => {
		console.log('Comming soon');
	};

	handleAddDestinationClick = (e) => {
		console.log('Comming soon');
	};

	handleAddPassangerButton = (e) => {
		console.log('Comming soon');
	};

    handleStartTripClick = (e) => {
        console.log('Comming soon');
	};

    handleCancelTripClick = (e) => {
        console.log('Comming soon');
    };

	render() {
		return (
			<section className="ControlsPanel__Wrapper">
				<section className={`TransportInfo ${this.props.selectedStation ? 'visible' : 'hidden'}`}>
					{
						this.props.selectedStation ? this.renderOptionsPanel() : null
					}
				</section >
				<section className="ControlsPanel">
					<section className="ControlsPanel__Item">
						<button className="AddPassangerButton" type="button" onClick={this.handleAddPassangerButton}>
							<i className="fa fa-user-plus" aria-hidden="true"></i>
						</button>
					</section>
					<section className="ControlsPanel__Item">
						<h4 className="ControlsPanel__Title">Trip Details</h4>
						<div className="TripDetails">
							<span className="AddTripLabel">Origin</span>
							<button className="AddTripButton" type="button" onClick={this.handleAddOriginClick}>
								<i className="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
						<div className="TripDetails">
							<span className="AddTripLabel">Destination</span>
							<button className="AddTripButton" type="button" onClick={this.handleAddDestinationClick}>
								<i className="fa fa-plus" aria-hidden="true"></i>
							</button>
						</div>
					</section>
					<section className="ControlsPanel__Item">
						<h4 className="ControlsPanel__Title">Selected Plan</h4>
						<ul>
							{this.state.selectedTransport.map((transport, index) =>
								<li key={`${transport.name}-${index}`}
									data-transport={transport.name}
									className={`${transport.selected ? "active" : "inactive"}`}
									onClick={(e) => this.handleClickOnCategory(index, e)}>
									<i className={`fas fa-${transport.icon}`}></i>
								</li>)
							}
						</ul>
					</section>
					<section className="ControlsPanel__Item">
						<button className="TripCancelButton" type="button" onClick={this.handleCancelTripClick}>Cancel</button>
						<button className="TripStartButton" type="button" onClick={this.handleStartTripClick}>Start</button>
					</section>
				</section >
				<section className="ControlsPanel__EventLog">
					<section className="ControlsPanel__Item">
						<h4 className="ControlsPanel__Title">Event Log</h4>
						<ul>
							{this.state.logs.map((log, index) =>
								<li key={`${log.text}-${index}`}
									className={`${log.status}`}>
									<i className={`fas fa-circle`}></i>
									{log.text}
								</li>)
							}
						</ul>
					</section>
				</section>
			</section>
		);
	}
}

export default ControlsPanel;